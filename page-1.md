# ⚖ How to Interview a CI

### Objective

This is the protocol for how we Interview the impacted in a Consent Incident.

### Protocol

* Begin
  * When contacted by First Responder
* Ends
  * File CIR

#### C. When the Interviewer arrives they will listen to/read what the person has to say, validate their concern, and offer to take a formal statement. <a href="#user-content-c-when-the-interviewer-arrives-they-will-listen-toread-what-the-person-has-to-say-valid" id="user-content-c-when-the-interviewer-arrives-they-will-listen-toread-what-the-person-has-to-say-valid"></a>

1. Stay calm and listen openly to the person while they are talking.
2. If appropriate to the violation, or when requested by the Reporting Party, the Interviewer will contact the Rangers on their behalf and escort them through the process of talking with the Rangers.
   * If a Ranger is called, contact the Camp Lead on duty immediately
3. Should someone report and request support for a Consent Violation that did not happen at an event and did not include someone connected to the organization we will do our best to listen and provide resources.
4. Should someone report a Consent Violation that happened outside of Brulee that involves someone officially connected to either organization (Board Member, Camp Lead, Camp Mate, or Volunteer) we will take that report as outlined in the below procedure and treat it as though it happened at an organization event.

#### D. Should the Reporting Party wish to make a formal statement the Interviewer will help them through the process, getting the information they are willing to relate. <a href="#user-content-d-should-the-reporting-party-wish-to-make-a-formal-statement-the-interviewer-will-help" id="user-content-d-should-the-reporting-party-wish-to-make-a-formal-statement-the-interviewer-will-help"></a>

1. Let the Reporting Party know you are following procedure and you will be asking them questions.
   1. Let the person know they can ask questions in return.
   2. Be honest with what you know and what you don’t.
2. Let the Reporting Party know what happens after you are done taking their report.
   1. A Camp Lead, or Consent Advocate will talk to the Alleged Offender(s).
   2. A summary of what happened will be generated.
   3. The report and summary will be reviewed by a person designated and trained to deal with consent violations.
   4. Recommendations for any actions to be taken will be made.
   5. Brulee will let them know when recommendations are made.
3. Let them know that their name will be held in confidence.
4. Fill out an Incident report form
   1. Collect names where possible, legal and/or scene names.
   2. Fill out as much of the form as you can based on what the person tells you.
   3. Fill out the time & date the report was taken, the time & date the violation/injury happened, and the location where it happened.
   4. Ask if it is okay to contact them for additional information at a later date.
   5. If there are witnesses that voluntarily come forth and want to give a statement, take those statements.
      * There is a place on the Incident Report for them. Where necessary write on a separate sheet and staple to the report.
      * If there were camp mates or volunteers present take their names and ask them if they have anything to add.
   6. After you finish talking to the Reporting Party, and where you feel safe to do so, approach the Alleged Offender and ask them if they would like to make a statement.
      1. There is a place on the Incident Report form for taking that statement.
      2. Understand this person is likely to be upset, confused, scared, angry, or all of the above. Avoid accusatory and judgmental language.
      3. Reassure the Alleged Offender:
         * You are not taking sides. It is your job to collect information and you want to get their side of what happened.
         * Information will be held in confidence by the organization through the review process.
      4. Answer any questions they have to the best of your ability.
      5. Let them know what happens next, same as above
      6. On your discretion you may ask the Alleged Offender to leave the event if:
         * The Consent Violation warrants such in your opinion.
         * If the person’s behavior was, is, or becomes threatening.
         * If your safety, the safety of the Reporting Party, or anyone else’s safety is in question.
      7. If either the Reporting Party or the Alleged Offender is not available, unapproachable, or would prefer, statements can be taken at a later time.

#### E. Resources <a href="#user-content-e-resources" id="user-content-e-resources"></a>

1. Resources and support can be facilitated by contacting the Rangers

#### F. Once the Interviewer fills out an Incident Report, it will be put it in a sealed envelope and either given to the Consent Lead in person or the camp lead on duty. <a href="#user-content-f-once-the-interviewer-fills-out-an-incident-report-it-will-be-put-it-in-a-sealed-envel" id="user-content-f-once-the-interviewer-fills-out-an-incident-report-it-will-be-put-it-in-a-sealed-envel"></a>

1. Inform the Consent Lead or Camp Lead on duty immediately that a report has happened.
2. Online CIRs do not require an envelope.
