# ⚖ How to Exclude Somone

### Objective

In the unfortuate situation where someone needs to be excluded from our community for whatever reason, this is a Protocol about how to do that fairly and empathetically.

### Protocol

This protocol needs to have things like how we inform people they are on a "list", where we keep that "list", how we maintain that "list", etc.

If we have a protocol for sharing that information, we need to document that as well and make sure that we have informed the individual of that protocol.
