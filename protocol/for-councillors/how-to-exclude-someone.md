# ⚖ How to Exclude Someone

### Objective

This is the protocol for how to exclude someone from our community.

### Protocol

* Begins
  * With a recommendation by the CIR Council to Exclude someone.
* End
  * With the individual being Excluded
  * Excluded name goes on a List
