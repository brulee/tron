---
description: This Protocol takes effect after the CI has been reported.
---

# ⚖ How to Review a CIR

### Objective <a href="#user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to" id="user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to"></a>

This is a protocol for the Consent Lead to follow after a Consent Incident has been Reported.  It contails guidelines for how to conduct a review.

### Protocol

* Begins
  * After CIR is reported
* End
  * Is the CIR actionable?
  * If the CIR requires Council, with formation of a CIR Council
  * If the CIR does not require Council, the Consent Lead is authorized to close the incident report without further review.

#### Once a CIR is received, the first step is for the Consent Lead to determine if the CIR is actionable. <a href="#user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to" id="user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to"></a>

1. Please review our Etiquette on [What is Actionable?](../../etiquette/what-is-actionable.md)
2.  If the CIR is not actionable, the issue should be closed with comment as to why it is not actionable.

    ![](<../../.gitbook/assets/image (3).png>)

#### If the CIR is actionable, the Consent Lead will perform a review or designate someone to perform a review. <a href="#user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to" id="user-content-g-once-a-cvr-is-received-the-consent-lead-will-perform-a-review-or-designate-someone-to"></a>

1. This review will include:
   1. A reading and filing of the given Incident Report and any supporting documentation.
   2. Debriefing the person who took the report for any additional information or impressions.
   3. A review of the current Master CIR List to see if there are any previous reports regarding the same persons.
   4. Contacting the Reporting Party, if they’ve given permission, to get any additional information.
   5. Contacting the Alleged Offender, should it seem necessary, to get any additional information.

#### Creation of a brief summary of what happened and the information collected.

1. The Summary will go into a comment in the CIR system.
2. This Summary should be generated without names. Instead use “Reporting Party”, “Alleged Offender”, “Interviewer”, “Witness 1”, Etc.
3. The Summary should include:
   * Date, event name, location, and time the incident happened.
   * A basic description of what happened from the point of view of the Reporting Party.
   * A basic description of what happened from the point of view of the Alleged Offender (if available).
   * Where possible, include a brief summary of any witness statements, staff or volunteer impressions, and any previous reports made against the persons involved.

#### If the report involves anyone directly connected with Brulee or (Camp Mate, Camp Lead, Volunteer, Staff, Board Member, or anyone who the public will see as connected to Brulee) the Consent Lead should: - - -

* Where there is any perceived conflict of interest, immediately contact the CIR Review Committee to oversee the review process and handle making recommendations with oversight from the Consent Lead.
* Where there is any perceived conflict of interest, the CIR Committee must be made up of people who are not personally or professionally connected to either the Reporting Party or the Alleged Offender.

#### The Consent Lead or designated reviewer will disclose the summary and recommendations to a designated CIR Council <a href="#user-content-h-the-consent-lead-or-designated-reviewer-will-disclose-the-summary-and-recommendations" id="user-content-h-the-consent-lead-or-designated-reviewer-will-disclose-the-summary-and-recommendations"></a>

1. The CIR Council will be made up of a minimum of three people drawn from members from the Board of Directors, Camp Leads, and/or trained staff or volunteers.
   * The Consent Lead will make a comment on the incident report that references the Councillors:
     * ![](<../../.gitbook/assets/image (1).png>)
     * These individuals should receive notifications and comment to accept the job.&#x20;

2\.  Alleged Offender.

* Any Advocate involved in a support role.
* The Board, coordinators, and/or other staff only where necessary to understand and/or enforce the actions taken.

4\.  The CIR  Council will close the review process once all parties have been notified.

* A single copy of the Incident Report, Summary, Statements, and Actions taken will be given to the Consent Lead for storage.
  * The Consent Lead will store the information in the appropriate locked location, filed by the Alleged Offender’s name, and update the Master CIR List.
  * &#x20;All other copies of the information should be destroyed
* A report of the number of Incidents reviewed, actions taken, and potential public relations issues will be given to the Board at the Board Meeting by the chair of the CIR Council.
  * Where names or specific incidents need to be discussed the board may choose to do that in executive session.
* Anyone involved can request a formal review or reassessment of the recommendations, summary, or decisions made.
  * The Director will forward the request to the standing CIR Council.
  * When forwarded, the Committee will review the request and/or initial documents. After that review they will either confirm the original statement and decisions or issue a new statement and decisions per the above procedure.
  * The request, any statements, changes made, and/or any other documentation will be given to the Consent Lead to be appended to the original file.

#### I.  Other Considerations: <a href="#user-content-i-other-considerations" id="user-content-i-other-considerations"></a>

1\.  Concerning Restraining Orders:

* Legal restraining orders are issued after careful consideration by a judge for a variety of reasons and prevent one party from being within a certain radius of and/or interacting with another party.
* If a person has a restraining order issued against them, that person should avoid attending any event attended by the person for whom the restraining order is issued and is held responsible for following the order issued against them.
* If both people have restraining orders, neither should attend any event until the orders are lifted.
* Brulee will not get involved in any dispute over a restraining order. Should such a dispute happen both parties will be asked to leave the event and associated premises.
* If, when asked to leave due to a restraining order issue, the person refuses to leave or if it seems clear they were there intentionally, this will be considered a consent violation and the above procedures will be followed.

If the Consent Violation is a Self-Report:

* We recognize not all Consent Violations are intentional. They can happen due to miscommunication, misunderstanding, or simple accident. Some non-consensual behaviors occur due to ignorance, cultural differences, and/or misinformation.
* We want to encourage people in acknowledging their own mistakes and seek help in changing difficult, non-consensual, or dangerous behavior.
* When the Consent Violation is reported by the person who committed that violation, the reporting procedure outlined above will be followed with the following differences:
  * &#x20;Extra attention should be paid to reassure the person reporting that we appreciate their coming forward themselves.
  * The Interviewer and Consent Lead should continue to maintain a nonjudgmental stance and language.
  * The person who experienced the violation will be approached carefully and asked if they would like to participate in the report.
  * Both parties will be given resources per the basic procedure. Educational resources should be highlighted for the person who self-reported.
  * Any review and recommendations should take into account the self-report and lean towards ways to educate and correct.
  * Even when there is a self-report, it doesn’t excuse the non-consensual behavior. It is still important to focus on and protect the safety of the person who experienced the violation.

#### J.  This Procedure is a living document and will be reviewed and updated as appropriate.
