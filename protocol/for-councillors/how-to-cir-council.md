---
description: Consent Incident Report Council UnSupervised
---

# ⚖ How to Resolve a CIR

### Objective

This protocol is for the CIR Council to Resolve a Consent Incident Report.

### Protocol

* Begin
  * After Consent Lead creates a CIR Council
* End
  * CIR is closed with comment explaining why or
  * Recommendations for further action.

#### The Consent Lead will disclose the summary and recommendations to a designated CIR Committee

* The Consent Lead will stay in the loop to guide the Council, but has no vote in their recommendations.&#x20;

#### The CIR Committee will take in the summary & recommendations, discuss these, and ask any additional questions.

#### CIRC Guiding Principles

1. Leadership are never exempt from Etiquette and Protocol. Indeed, we should hold them to higher standards.
2. We do not adjudicate truth. Assume the worst.
3. We only Council for Brulee Spaces. Different Spaces have different etiquette and protocol.
4. We are not relationship councillors. Relationships are their own Space, and have their own Etiquette and Protocol.
5. Only 1st party reports are actionable. We need to value impacted party's rights and privacies.
6. Sometimes _relationships_ are toxic to the community. Without judging right and wrong, sometimes it is best for the community to exclude both parties in a relationship.
7. Immediate threats to individuals and the community should be removed immediately. \[tbr]
8. If laws are broken, encourage the Impacted to contact authorities.
9. If Etiquette and Protocol are at all unclear - if it's not clear what you should do - create an Issue describing the difficulty.
10. Pay special attention to whether the Implicated (and Impacted) are aware of our Etiquette and Protocol. If they are unaware, we want to bring community attention to this so we can improve our Radical Communication component. Our Etiquette and Protocols are NOT obvious, are likely different from other communities and must be explicitly taught.
11. We don't Council Incidents that are more than a year old.

#### Some Thoughts about Severity

When working on these documents we found that there was lot of discussion and concern around how to quantify the level or severity of a consent violation. This is a place people go to for a lot of different reasons. Sometimes they want to know how outraged they’re supposed to be. Sometimes they want to know the severity so they can determine how to either punish or defend the people involved. And sometimes they ask the question in order to deflect or deal with their own discomfort about what happened.

In the process of writing about Consent and Consent Violations we have come up with the following ideas. We know they’re neither perfect nor comprehensive. And we hope they can give some grounding and basis for further discussions.

* We acknowledge the only person who can qualify the level of a consent violation or injury is the person who experienced it. No one should ever attempt to convince another that a given experience is either less or more significant. _**\[Should we then not include any determination of severity in this Protocol for CIRC?]**_
* We understand the perception of the level of violation/injury may differ between the people involved and recognize that this is normal, complicated, and doesn’t need to be changed. It is okay for different people to have different perceptions.
* We know that we cannot allow the rights and needs of a person reported against to supersede, overwhelm, or overshadow the rights and needs of a person who reports. At the same time we recognize a person reported against has needs as well and want to do our best to help them understand what happened and find ways to prevent it from happening in the future.
* Finding a balance between creating safety, managing confidentiality, supporting those that have suffered harm, and working to repair connections within the community is not easy work. And we believe it is possible with time, compassion, understanding, and patience.
* We acknowledge there are different levels of consent violation/injury and that these need to be handled differently.
  * &#x20;Accidental Violations are non-consensual actions that happen without the intent to harm and which may happen within the normal course of human interaction. Such an event, regardless of the intent, may still cause physical, emotional, or psychological impact. It should be reported and supported for both the person who experienced it and the person reported against.
    * Basic examples (not meant to be a complete list): An unwanted hug, standing too close to someone, pushing against a stated boundary, following someone through an event or space, etc.
    * Working with these types of violations/injuries is about working with, understanding, and acknowledging the emotions of the people involved and pursuing reparative actions to address the impact of what happened.
  * Serious Violations are non-consensual actions, that may or may not happen with the intent to harm, which have a significant physical, emotional, or psychological impact on an individual.
    * Such an event should always be reported and the person who experienced the violation/injury supported in that reporting. The person reported against may also need support, but safety must be the primary priority.
    * &#x20;Basic examples (not meant to be a complete list): Not stopping immediately when perceiving a safeword, not following negotiated safer sex protocols, breaking negotiated boundaries or agreements, etc.
    * &#x20;Intentionally acting to remove or reduce someone’s ability to consent will always be considered a serious violation.
    * Working with and acknowledging Serious Consent Violations is complicated, serious, and necessary work. It is first about safety for the person reporting and the community as a whole.
  * Legal Violations are non-consensual actions for which there are legal statutes against them and/or for which someone has pursued legal action.
    * There are many laws and legal statutes that determine what is and is not a violation pursuable under the law. We recognize that BDSM, Kink, and Sex-Positive Culture create difficulties and oppressions under the law and this often makes pursuing legal action difficult.
    * We have the responsibility to support anyone who has experienced a consent violation/injury to contact the police, should they feel the need to do so, and to support them in through the emotional process of dealing with the authorities.

#### The CIR Comittee will respond witha a comment with a list of actions to be taken.

* [ ] File the report and take no further action.
* [ ] &#x20;Contact the Reporting Party and offer additional support and/or resources by contacting the Rangers.
* [ ] Contact the Alleged Offender and offer additional support and/or resources by contacting the Rangers.
* [ ] Offer Mediation to both parties to address confusion, misunderstandings, miscommunications, and/or emotional distress. – Both parties must freely agree to this course of action.
* [ ] &#x20;Give a verbal or written warning to the Alleged Offender.
* [ ] &#x20;Impose a temporary ban from Organization Events on the Alleged Offender.
* [ ] Impose a temporary ban with a list of conditions which the Alleged Offender must fulfill before being allowed to return to Brulee Events.
* [ ] &#x20;Impose a permanent ban from all Brulee Events on the Alleged Offender.
* [ ] &#x20;Make other suggestions as warranted by the Consent Violation.

#### The CIR Council will respond with a comment of how what happened was against the Consent Policy (if it was).

#### The CIRC will respond with a  comment of what could have been done differently to comply with the Consent Policy.

#### The CIRC will respond with a comment of how to request a review or reassessment

#### Results of the CVR Review Committee’s review created will be offered to all necessary parties including:

1. The Interviewer and Consent Lead or designated Reviewer.
2. The Reporting Party and the Alleged Offender.
3. Any Advocate involved in a support role
4. The Board, coordinators, and/or other staff only where necessary to understand and/or enforce the actions taken.

#### After everyone has been informed, the CIRC will close the Issue

![](<../../.gitbook/assets/image (3).png>)

#### Reopening the Issue

1.  Anyone involved can request a formal review or reassessment of the recommendations, summary, or decisions made.

    1. The Consent Lead will reopen the Issue and forward the request to the standing CIR Committee.
    2. When forwarded, the Committee will review the request and/or initial documents. After that review they will either confirm the original statement and decisions or issue a new statement and decisions per the above procedure.

    ![](<../../.gitbook/assets/image (6).png>)
