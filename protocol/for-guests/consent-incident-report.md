# 📋 Consent Incident Report

The online form always lives at [http://brulee.co/cir](http://brulee.co/cir)

### Purpose

This form is for reporting behavior that is not in alignment with the agreed upon Etiquette and Protocol of our community. It gives you a record of having reported it, and ensures that we have a record of the incident.

### Form

#### RequiredEmail&#x20;

_We use this email address below to keep you informed of incident progress. If you wish to remain completely anonymous, you can just use_ [_anon@brulee.co_](mailto:anon@brulee.co) _as the email address. Anonymous reports are not immediately actionable._

> __

#### Who are you?

> __

#### Who are you in relation to this Incident?

_In general, out of respect for the impacted, only 1st person reports are actionable. That said, we value witnesses, upstanders and co-strugglers and will use your Report to help make our community healthier. We cannot improve what we don’t know._

* [ ] My consent was violated
* [ ] I violated our Etiquette & Protocol
* [ ] As an “upstander”, I have witnessed the incident or are submitting this on someone’s behalf.
* [ ] As a “co-struggler”, I somehow know of this incident.
* [ ] Anonymous. I do not wish to tell us who you are, but want this information to be known.
* [ ] Other:

#### Where did this incident happen?

&#x20;_In general, only incidents that happen in Brulee are reportable. That said, we welcome any and all information we can use to make our community safer._

* [ ] In Brulee, Black Rock City
* [ ] In Brulee, Apogaea
* [ ] In Brulee, at a camp meeting
* [ ] In Brulee, somewhere else
* [ ] Other:

#### When did the incident happen

_In general, incidents that happened more than a year ago are not actionable. That said, we value every incident report as it helps us to grow and evolve our Etiquette & Protocol._

> __

#### This incident was...

_Incidents more than a year old are generally not actionable beyond being informational._

* [ ] within the past year
* [ ] more than a year ago.

#### Who was Impacted by this Incident?

>

#### Who is Implicated in this Incident?

_As a general rule, if you are the Impacted Party and are reporting someone, our protocol is to contact the Implicated parties to interview them about their role in the incident.  If you are not the Impacted Party, we will take this issue confidentially and no action will be taken_

>

#### What happened?

> &#x20;
>
> &#x20;
>
> &#x20;
>
> &#x20;
>
> &#x20;
>
>
