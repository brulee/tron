---
description: Protocol for the CIR, describing the fields.
---

# ⚕ How to Report a CI (CIR)

### Objective

This is the protocol for how you go about reporting a Consent Incident.

### Protocol

Canonical link to CIR: [http://brulee.co/cir](http://brulee.co/cir)

If online is not easily available, we will have one page CIR forms to fill out.
