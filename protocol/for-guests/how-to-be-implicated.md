# 💞 How to \_Be\_ Implicated

### So you have been implicated in a Consent Violation...

1. **Don't Panic** It is natural to feel defensive whenever you are accused of anything. Trust the Protocol. You will have ample opportunity to defend yourself later.
2. **Listen** This isn't a time for responding. That time will come. This is a time to listen. If you are speaking, you are probably not thinking. Listen carefully, think about what happened. Take notes if you like.
3. **Be Respectful** When you do respond, be respectful. Be respectful of the Impacted party and be respectful of the Councillor you are speaking with. Don't raise your voice. They aren't out to get you.
4. **Be Honest** If you are reading this, you probably made a mistake. Being honest allows you and us to better understand where that mistake was. This is an opportunity for you to learn and for the community to learn.
5. **Be Vulnerable** We are trying to create a safe space for our community. Part of this is trusting our community.
6. **Be Compassionate** Appreciate that someone you had a relationship with (however casual or formal) has been impacted by your actions. Again, while it's natural to be defensive, right now the impacted person needs your compassion.

#### Your Advocate <a href="#user-content-your-advocate" id="user-content-your-advocate"></a>

You will be assigned an Advocate Councillor who will help you through this process and will advocate for you in the Incident Report thread if you like. This is likely to be the Councillor who has contacted you, but may not be.

#### The Consent Incident Report <a href="#user-content-the-consent-incident-report" id="user-content-the-consent-incident-report"></a>

The CIR is where all interactions on the incident will be documented. Your Advocate can represent you in the Incident Report thread. You will be provided a link to the Incident Report. If you register for a GitLab account, you can request to be added to the thread directly and advocate for yourself, or your Advocate will keep you in the loop.

#### Confidentiality <a href="#user-content-confidentiality" id="user-content-confidentiality"></a>

Confidentiality is always assumed. The Incident Reporter obviously knows about this. That may or may not be the Impacted party. Your Councillor knows about this. Other Councillors will be added to create a Council as needed and resolve the issue.

When all is resolved, we will possibly anonymize the case and add it to our abUse Case to communicate it so the community can learn and evolve.
