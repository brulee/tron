# CIR First Responder
Any one in Brulee might be a First Responder.  Please review our [Protocol for First Responders](../wikis/Consent%20Incident%20First%20Responder).  First Responders might have created this report, or guided someone to do so.  In general, First Responders do not need to edit this (and might not have the ability to).  This section is to capture what First Responder did.

Did the First Responder
 - [x] Create an Consent Incident Report.
 - [ ] Contact Implicated Party?
 - [ ] Contact Consent Lead?
 - [ ] Contact Rangers?
 
## Interviewer / Consent Advocate

## Other Communication
Has the impacted party spoken to the implicated party?
- [ ] No
- [ ] Yes

Does the impacted party want the implicated party to be contacted?
- [ ] Yes
- [ ] Not Yet
- [ ] Not Yet, Please mediate

## Contact Rangers
- [ ] Contact Rangers if:
  - [ ] You feel the issue is serious.
  - [ ] You feel someone is in danger.
  - [ ] You think laws may have been broken.
- [ ] Rangers were contacted.

# Consent Lead Review
This section to be checked by Consent Lead or Deputy thereof.  Consent Lead should 

1. Is this actionable?
- [ ] CIR is actionable if:
  - [ ] the incident happened in a Brulee Space
  - [ ] the incident involves a Brulee Councillor
  - [ ] this is a First Party Report
  - [ ] the Impacted party is okay with talking to the Implicated party
- **IF NOT, stop here.  Close the issue.  Do not contact Implicated Party.**

2. Implicated Party Statement
- [ ] contact the Implicated party, take a statement
_fill in statement here_

3. Witness Statements
   - Witness 1
     - _fill in statements here_

   -  Witness 2
      - _fill in statements here_

4. If not Consent Lead is authorized to close the issue without further review.
  - [ ] Impacted Contacted
  - [ ] Implicated Contacted
  - [ ] Issue Closed

5. Organize Council Review
A council ultimately requires 5 councillors: 3 will adjudicate, an advocate for the Impacted and an advocate for the Implicated.
  - [ ] A: _councillor name_
  - [ ] B: _councillor name_
  - [ ] C: _councillor name_
  - [ ] Impacted Councillor: _councillor name_
  - [ ] Implicated Councillor: _councillor name_

6. Council Review Objective
This is where you, the Consent Lead, tasks the Council with what to review.
The Council should decide:
  - [x] Was our Etiquette & Protocol Violated?


# Council Review
This section should be populated by Councillors.

0. Principles
  - Remember, we are looking to keep our Spaces safe.
  - We are not looking to decide truth.
  - We are not relationship councillors.

1. Actionable
These items should have been checked before going to Council. They shouldn't really require debate. If these are not obvious, please consult the Consent Lead.  
- [ ] Incident happened in Brulee Space
- [ ] Incident happened within last year
- [ ] Reporter is an Impacted party
- [ ] Impacted Party has been contacted
If these are not all checked, close the issue - it's not for Council to discuss.

2. Review the report and above sections
  - [ ] A: reviewed
  - [ ] B: reviewed
  - [ ] C: reviewed

3. Objectives (per Consent Lead)
- [ ] Our Etiquette & Protocol was violated.
_List violation here_
  - [ ] A: yes
  - [ ] B: yes
  - [ ] C: yes

- [ ] Incident is Serious Violation
  - [ ] A: yes
  - [ ] B: yes
  - [ ] C: yes

4. Council Recommendations
- [ ] File the report and take no further action.
- [ ] Contact the Reporting Party and offer additional support and/or resources by contacting the Rangers.
- [ ] Contact the Alleged Offender and offer additional support and/or resources by contacting the Rangers.
- [ ] Offer Mediation to both parties to address confusion, misunderstandings, miscommunications, and/or emotional distress. – Both parties must freely agree to this course of action.
- [ ] Give a verbal or written warning to the Alleged Offender.
- [ ] Impose a temporary ban from Organization Events on the Alleged Offender.
- [ ] Impose a temporary ban with a list of conditions which the Alleged Offender must fulfill before being allowed to return to Brulee Events.
- [ ] Impose a permanent ban from all Brulee Events on the Alleged Offender.
- [ ] Make other suggestions as warranted by the Consent Violation.

5. Consent Lead Review
- [ ] Consent Lead: Are these recommendations acceptable?

# Appeal
Council actions may be appealed.

Upon submission of this form, it will go to the [Consent Incident Report Committe](https://gitlab.com/-/ide/project/brulee/tron/tree/main/-/EP.md/#consent-incident-report-comittee-circ) for review.
