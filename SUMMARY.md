# Table of contents

* [Welcome](README.md)

## 📜 Etiquette

* [📜 Consent Etiquette](etiquette/readme.md)
* [📜 What is Actionable?](etiquette/what-is-actionable.md)
* [📜 List of Potential Actions](etiquette/consequences.md)

## ⚖ Protocol

* [⚕ for Guests](protocol/for-guests.md)
  * [💞 How to be an Upstander](protocol/for-guests/how-to-be-an-upstander.md)
  * [💞 How to Cosuffer](protocol/for-guests/how-to-cosuffer.md)
  * [⚕ How to Respond to a CI](protocol/for-guests/how-to-respond-to-a-ci.md)
  * [⚕ How to Report a CI (CIR)](protocol/for-guests/how-to-report-a-ci-cir.md)
  * [📋 Consent Incident Report](protocol/for-guests/consent-incident-report.md)
  * [💞 How to Be Implicated](protocol/for-guests/how-to-be-implicated.md)
* [⚖ for Councillors](protocol/for-councillors/README.md)
  * [⚖ How to Interview a CI](page-1.md)
  * [⚖ How to Review a CIR](protocol/for-councillors/how-to-review-a-cir.md)
  * [⚖ How to Resolve a CIR](protocol/for-councillors/how-to-cir-council.md)
  * [🎓 How to Be a Councillor](protocol/for-councillors/how-to-be-a-councillor.md)
  * [⚖ How to Exclude Someone](protocol/for-councillors/how-to-exclude-someone.md)
  * [💞 How to Mediate](protocol/for-councillors/how-to-mediate.md)
  * [💞 How to Interview](protocol/for-councillors/how-to-interview.md)
  * [💞 How to Advocate for the Impacted](protocol/for-councillors/how-to-advocate-for-the-impacted.md)
  * [💞 How to Advocate for the Implicated](protocol/for-councillors/how-to-advocate-for-the-implicated.md)
  * [⚖ How to Exclude Somone](protocol/for-councillors/how-to-exclude-somone.md)

***

* [🎓 Education](education/README.md)
  * [❗ Class 1 Education](education/class-1-education.md)
  * [❓ Class 1 Quiz](education/class-1-quiz.md)

## TRON

* [🤖 TRON](tron/tron.md)
* [🤖 C-3PO](tron/c-3po.md)

## Brulee

* [brulee.co](http://brulee.co)
* [brulee.co/cir](https://brulee.co/cir)

***

* [📑 Glossary](glossary.md)
* [🧐 How to Get Consent](how-to-get-consent.md)
* [🧩 Brave New Safe Spaces](brave-new-safe-spaces.md)

## Group 1
