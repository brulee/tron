# 📑 Glossary



#### Organization

* Brulee.

**Space**

* Any event, activity, or offering put on, hosted by, or sponsored by Brulee.

#### Consent

* See Consent Etiquette The explicit or implicit expression made by a person that they are willing to have something done to them by one or more other persons, or that they are willing to perform an act at the request or order of one or more other persons.

#### Consent Violation

* An incident during which
  * a person is acted upon (or is coerced to perform an act) by another or by others in a manner not consented to in advance or
  * a person is acted upon (or is coerced to perform an act) by another or by others after a withdrawal of consent is made by the acted-upon person.
* A Consent Incident may occur accidentally or intentionally, with or without malice, and may or may not have injurious consequences.
* A Consent Incident may be physical, emotional, mental, or social in nature.
* A Consent Incident may be obvious to all involved, realized by a subset of the people involved, or only be clear to a single individual.

#### Consent Incident Report (CIR)

* A notice that a Consent Incident has taken place. This notice may be made in-person or by other means, such as by telephone or e-mail. Formal CIRs include an Incident Report and other documentation as appropriate.

#### Reporting Party

* The person who is making the CIR.

#### Impacted

* The person who was impacted in the Consent Incident

#### Implicated

* The person (or persons) named in the report as having committed the subject Consent Violation.

#### First Responder

* The Organization staff member (Consent Advocate) with whom the Reporting Party initially makes contact.

#### Interviewer

* The person formally receiving and recording the CIR on behalf of Brulee. This is always the senior staff member on duty.

#### CIR Council

* A committee convened by the Board of Directors to make final adjudications regarding CIRs and to review Brulee’s handling of the CIR.
* The Consent Lead is a informal member of an CIRC to make sure it is run properl

#### CIR Council UnSupervised

* Once we have sufficient Protocol in place, CIRC's can happen without explicit Consent Lead participation.

#### Consent Advocate

* An individual who has been trained in our Etiquette and Protocol

#### Consent Advocate Resource Network

#### Consent Etiquette & Protocol Officer (C-3PO)

#### Impacted Advocate

* A professional or trained individual whose role is to support the Reporting Party, with their consent and involvement, through the CIR process and to support them with the mental, emotional, and logistical difficulties that arise from a CI.

#### Implicated Advocate
