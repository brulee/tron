# 📜 List of Potential Actions

This is list of potential Actions for any Concent Incident Report

* [ ] File the report, close the issue, and take no further action.
  * Be sure to comment as to why no further action is being taken
* [ ] Contact the Reporting Party and offer additional support and/or resources by contacting the Rangers.
* [ ] Contact the Alleged Offender and offer additional support and/or resources by contacting the Rangers.
* [ ] Offer Mediation to both parties to address confusion, misunderstandings, miscommunications, and/or emotional distress. – Both parties must freely agree to this course of action.
  * This requires Protocol for how we mediate.&#x20;
  * We are not relationship coucillors.
* [ ] &#x20;Give a verbal or written warning to the Alleged Offender.
  * Both verbal and written warning should be commented on the CIR
* [ ] Impose a temporary ban from Organization Events on the Alleged Offender.
  * This requires it's own [Protocol](../protocol/for-councillors/how-to-exclude-someone.md)
* [ ] Impose a temporary ban with a list of conditions which the Alleged Offender must fulfill before being allowed to return to Brulee Events.
  * This requires it's own [Protocol](../protocol/for-councillors/how-to-exclude-someone.md)&#x20;
* [ ] &#x20;Impose a permanent ban from all Brulee Events on the Alleged Offender.
  * This requires it's own [Protocol](../protocol/for-councillors/how-to-exclude-someone.md)&#x20;
* [ ] Require online Consent Classes (see: [Education](../education/))
* [ ] Make other suggestions as warranted by the Consent Violation.
