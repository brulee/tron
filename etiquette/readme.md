# 📜 Consent Etiquette

### How do we define consent?

#### What is Consent?

Consent is voluntary, positive agreement between all participants to engage in specific activity.  PERIOD.  If the below hasn’t been followed, then it’s not consensual.  As such, there are a few minimums for someone to be able to consent to an activity:

* All parties have been well informed of the discussed activities – you can’t rightly agree to that which you don’t know
* All parties accept responsibility for communication, risk, and outcomes – voluntary positive agreement means mutual responsibility for consequences
* Has a time frame or limit for when and where activities will take place – is properly planned for and will not be random or ad-hoc (particularly for psycho-emotional S\&M)
* All parties have explicit permission to engage in the agreed activities – no one is participating if they aren’t on the same page and in agreement
* Agreements made of sound mind (without impairment of alcohol, drugs, medications, etc.)
  * If you aren’t of sound mind, then you can’t consent to BDSM/Sexual Play activities.  May also include sleep deprivation and other factors which can cause impaired judgment.  Please note that a person’s state of mental impairment _is not a defense_ for having committed assault.

Our understanding of consent is this:

* The act of getting consent is the act of making sure everyone involved in a given activity is able to give their ok, understands what’s going on, and directly, consciously, and willingly chooses to engage in that activity.
* The basics of consent are simple: Get full and informed consent, with awareness of the explicit and implicit consent you are using before you engage in an activity with someone.
* The application of consent is complex, emotional, and takes practice.

We realize it is not enough simply to state that consent is important. To build a **consent culture**, we need to both talk about consent in detail and improve the safety of the people we camp with and serve. It is not enough to sit on the sidelines and watch as events unfold around us. It is our right and our responsibility to find better ways to talk about, explore, and protect consent.

We recognize that we are, and should be, held to a high standard of understanding and enforcing consent. This statement and associated policy and procedure/protocol and etiquette documents are the culmination of our current discussions and understanding. These are living documents and will grow and change as we learn and understand more.

#### Why It’s Important.

It’s not enough to talk about consent. It’s not enough to teach about consent. As a Camp that promotes explicit consent and the freedom of sexual expression, we recognize our responsibility in working to promote a safe, secure, and consensual environment at all of our events.

* It’s Brulee’s mission is celebrate and cultivate sexuality through education, art, and the radical self-expression.
* As proponents of sex positive culture, our goal is to create a better world by promoting respectful interaction through consent education. We aim to move society into acceptance of sexuality as a healthy, integral part of being human. We support people in finding the type and amount of sexual pleasure that is right for them, respecting the spectrum from asexual to omnisexual.

Part of our mission is the creation and promotion of a Consent Culture where people are able, welcome, and encouraged to openly and honestly ask for consent regardless of the desired activity. Where people are free and encouraged to respond based on their own choices and desires. And where everyone respects and honors the response given, regardless of the desired outcome.

Consent violations and injuries happen, and we are deeply concerned about the personal, communal, and cultural repercussions. We recognize the damage these do and are committed to finding ways to both reduce the number of incidents and to supporting people who have experienced them.

We believe that changing the culture starts with making consent the cornerstone of all our events, educational opportunities, and outreach. We believe that creating a **Consent Culture** takes all of us, working together to be personally responsible and to hold each other accountable.

And finally, we believe in working toward a time where people are so concerned with consent, that they can and will report their own violations; working with both the person who experienced the violation and the community to make amends.

#### What are we doing about it:

We have created a dynamic policy to lay out an ideological grounding for how we look at consent and consent violations. This policy will be posted in all of our events, listed on our websites, and made available to anyone who wants it. It will be reviewed and updated yearly or whenever new information comes to light.

### Brulee Consent Policy/Protocol and Etiquette

We have created a procedure to give step by step guidelines for how to deal with consent violations or injuries when they are reported to us. This detailed procedure will be taught to our staff and volunteers and available for anyone to look at. It will be reviewed and updated yearly or whenever new information comes to light.

### Brulee Consent Report Handling Procedure

1. As an organization we believe it is important to help people learn about consent and that through education we can help people to understand how important it is to engage in consensual behavior.
   1. We provide regular workshops on consent for camp mates, community, and at our events. We will also host additional workshops on the subject.
   2. We will engage in outreach to our Burner community and other sex positive organizations to support their consent curriculums and/or to offer ours.
   3. Brulee will be working to create a Consent Advocate Council. This program will be made up of trained people willing to be available at events for helping someone should a consent violation occur.
   4. Advocates will be expected to be available to be called in to support and help someone who has experienced a consent violation or injury while at an event hosted by Brulee. We will also keep a resource list of people willing to support anyone who contacts us looking for support.
   5. Individuals will be designated to be on staff at any large event. 1.The program will include training in Consent Advocacy: Connection and Validation Skills, Creating and Managing Safety, Supporting and Minimizing Trauma, Legal and Resource Support, and Restorative Justice Techniques (Inclusion, Encounter, Amends, & Reintegration).

### Discussion around the Level or Severity of Consent Violation:

When working on these documents we found that there was lot of discussion and concern around how to quantify the level or severity of a consent violation. This is a place people go to for a lot of different reasons. Sometimes they want to know how outraged they’re supposed to be. Sometimes they want to know the severity so they can determine how to either punish or defend the people involved. And sometimes they ask the question in order to deflect or deal with their own discomfort about what happened.

In the process of writing about Consent and Consent Violations we have come up with the following ideas. We know they’re neither perfect nor comprehensive. And we hope they can give some grounding and basis for further discussions.

* We acknowledge the only person who can qualify the level of a consent violation or injury is the person who experienced it. No one should ever attempt to convince another that a given experience is either less or more significant.
* We understand the perception of the level of violation/injury may differ between the people involved and recognize that this is normal, complicated, and doesn’t need to be changed. It is okay for different people to have different perceptions.
* We know that we cannot allow the rights and needs of a person reported against to supersede, overwhelm, or overshadow the rights and needs of a person who reports. At the same time we recognize a person reported against has needs as well and want to do our best to help them understand what happened and find ways to prevent it from happening in the future.
* Finding a balance between creating safety, managing confidentiality, supporting those that have suffered harm, and working to repair connections within the community is not easy work. And we believe it is possible with time, compassion, understanding, and patience.
* We acknowledge there are different levels of consent violation/injury and that these need to be handled differently.
  * Accidental Violations are non-consensual actions that happen without the intent to harm and which may happen within the normal course of human interaction. Such an event, regardless of the intent, may still cause physical, emotional, or psychological impact. It should be reported and supported for both the person who experienced it and the person reported against.
  * Basic examples (not meant to be a complete list): An unwanted hug, standing too close to someone, pushing against a stated boundary, following someone through an event or space, etc.
* Working with these types of violations/injuries is about working with, understanding, and acknowledging the emotions of the people involved and pursuing reparative actions to address the impact of what happened.
* Serious Violations are non-consensual actions, that may or may not happen with the intent to harm, which have a significant physical, emotional, or psychological impact on an individual.
  * Such an event should always be reported and the person who experienced the violation/injury supported in that reporting. The person reported against may also need support, but safety must be the primary priority.
  * Basic examples (not meant to be a complete list): Not stopping immediately when perceiving a safeword, not following negotiated safer sex protocols, breaking negotiated boundaries or agreements, etc.
  * Intentionally acting to remove or reduce someone’s ability to consent will always be considered a serious violation.
  * Working with and acknowledging Serious Consent Violations is complicated, serious, and necessary work. It is first about safety for the person reporting and the community as a whole.
* Legal Violations are non-consensual actions for which there are legal statutes against them and/or for which someone has pursued legal action.
  * There are many laws and legal statutes that determine what is and is not a violation pursuable under the law. We recognize that BDSM, Kink, and Sex-Positive Culture create difficulties and oppressions under the law and this often makes pursuing legal action difficult.
  * We have the responsibility to support anyone who has experienced a consent violation/injury to contact the police, should they feel the need to do so, and to support them in through the emotional process of dealing with the authorities.
* No volunteer or staff should ever attempt to discourage someone from making a report should they choose to do so. The level of violation is never an issue in whether or not a report should be made. Any attempt to discourage someone is grounds for dismissal.

The discussion around level, type, details, or even the occurrence of a consent violation is always difficult and it ultimately not the point. When someone experiences a consent violation that is their experience and it is our job, the job of everyone involved, to help them process what happened. We succeed and grow as a community by helping people heal, providing a safer space for everyone, and doing what we can to prevent violations from happening in the first place.

### **Brulee Consent Violation Decision Tree** (example below)

![image.jpeg](../image.jpeg)

**Consent Policy/Protocol and Etiquette** (sample)

It is everyone’s responsibility to understand, ask for, and choose whether to give or withhold consent. Together, we seek to create a safe and consensual space where people are accepted and able to explore themselves and their sexuality.

These are the consent guidelines we expect everyone at Brulee events to follow while you are with us. (We hope you will follow them outside our doors as well.):

1. No touching people or personal property without permission.
2. Treat everyone as an equal by default. Everyone has responsibility to obtain, provide, or withhold consent regardless of sex, gender, race, ethnicity, ability, age, orientation, relationship status, sexual power dynamics, or any other identity.
3. Negotiate the scope of your activities. We recommend making clear that all parties involved have given consent to the proposed acts.
4. Each participant is responsible for making sure, to the best of their ability, that everyone involved has the physical, mental, and emotional capacity to give informed and voluntary consent during negotiation and during the activity itself.
5. Anyone can withdraw consent at any time during any activity. Participants need to agree on meanings for safewords or safesigns when they are being used. All participants shall endeavor to be clear and unequivocal when withdrawing consent. (The house safeword is “Safeword”.)
6. All participants are responsible for stopping immediately any activity at the withdrawal of consent from anyone.
7. Unless previously agreed upon, we recommend not re-negotiating in the middle of an activity. When a person is not in a clear state of mind, you may not have full or informed consent even though they agree in the heat of the moment.

8\. **If your consent is violated or you experience a consent injury, verbally tell a Consent Advocate immediately.** There is a procedure in place to help you. It is your right to report what happened to you and to ask for support.

1. Violation of the consent policy may result in expulsion from the event and/or temporary or permanent ban from Brulee events event or activities. There are procedures in place to handle violation of this policy and those procedures will be followed by all volunteers, staff, and Consent Advocates. No one is exempt from the policy.
2. Disclaimer: Every reasonable effort will be made to enforce this policy, but neither organization makes representations or guarantees about its ability to do so. Each situation is distinct and will be reviewed on a case by case basis. All participants and attendees retain full, sole responsibility for their safety and the safety of others with whom they interact.

### Consent Violation Report Handling Procedure

This document provides the steps required to successfully receive, process, and resolve Consent Violation Reports made to the Brulee.

**Glossary of Terms**

\- **Organization**:  Brulee.

* &#x20;**Organization Event**:  Any event, activity, or offering put on, hosted by, or sponsored by the Brulee.
*   &#x20;**Consent**:  The explicit or implicit expression made by a person that they are willing to have something done to them by one or more other persons, or that they are willing to perform an act at the request or order of one or more other persons.

    * Consent includes the ability to make that expression, the conscious understanding of what is being done or requested, and the active willingness to engage.

    \- Consent applies to all persons involved in an activity regardless of role.

    \- Consent may be withdrawn at any point, regardless of prior negotiations between participating parties.
*   &#x20;**Consent Violation**:

    * &#x20;An incident during which
      * &#x20;a person is acted upon (or is coerced to perform an act) by another or by others in a manner not consented to in advance or
      * &#x20;a person is acted upon (or is coerced to perform an act) by another or by others after a withdrawal of consent is made by the acted-upon person.

    \- A Consent Violation may occur accidentally or intentionally, with or without malice, and may or may not have injurious consequences.

    * &#x20;A Consent Violation may be physical, emotional, mental, or social in nature.
    * &#x20;A Consent Violation may be obvious to all involved, realized by a subset of the people involved, or only be clear to a single individual.

\- **Consent Violation Report (CVR)**:  A notice that a Consent Violation has taken place.  This notice may be made in-person or by other means, such as by telephone or e-mail.  Formal CVRs include an Incident Report and other documentation as appropriate.

* &#x20;**Reporting Party**:  The person against whom the Consent Violation was committed and who is making the CVR.

\- **Alleged Offender(s)**:  The person (or persons) named in the report as having committed the subject Consent Violation.

\- **First Responder**:  The Organization staff member (**Consent Advocate**) with whom the Reporting Party initially makes contact.

* **Interviewer**:  The person formally receiving and recording the CVR on behalf of Brulee.  This is always the senior staff member on duty.
* **Incident Report**:  The form completed by the Interviewer when the Reporting Party chooses to make a formal CVR.

\- **CVR Review Committee**:  A committee convened by the Board of Directors to make final adjudications regarding CVRs and to review Brulee’s handling of the CVR.

\- **Consent** **Advocate**:  A professional or trained individual whose role is to support the Reporting Party, with their consent and involvement, through the CVR process and to support them with the mental, emotional, and logistical difficulties that arise from a CV.

### First Responder

1. &#x20;This procedure begins when anyone reports a Consent Violation to any Volunteer or Staff.
2. &#x20; A CVR should be addressed as quickly as possible.

```
1.   We recognize people may not feel safe in the moment to report and may need time to process emotional, psychological, and physical effects.
```

1. &#x20; Any report made after the event where the Consent Violation occurred will be referred to the Consent Lead. Reports may be emailed to tarabogle1@gmail.com

```
1.   The Consent Lead may designate another Interviewer.
```

```
1.   The process will then follow the normal reporting procedure.
```

3\.   Any CVR made after a year has elapsed from the time the Consent Violation occurred will be taken as an information only report to be added to the file.

```
1.   No action recommendations are necessary.

1.   The Interviewer will inform the Reporting Party that the report is being taken for informational purposes only.

1.   Should the Interviewer consider the violation to serious enough, or if there are multiple reports against the same person, the Interviewer may request further deliberation and/or make recommendations to the designated sub-committee.
```

1. &#x20;When the Consent Advocate is approached, in person or in writing, that person will listen to what the Reporting Party has to say, validate their concerns, and offer to escort or direct them to someone who can help.
2. &#x20;It is not your job to determine the validity of the report.
3. &#x20;Never try to convince someone they should not report.
4. &#x20;If an Interviewer is not immediately available contact a Camp Lead on duty to let them know they are needed.

```
-  Take the Reporting Party to a safe and quiet space and wait with them until the Interviewer is available.
```

* &#x20;When the Interviewer arrives they will listen to/read what the person has to say, validate their concern, and offer to take a formal statement.
  * &#x20;Stay calm and listen openly to the person while they are talking.
  * &#x20;If appropriate to the violation, or when requested by the Reporting Party, the Interviewer will contact the Rangers on their behalf and escort them through the process of talking with the Rangers.
  * &#x20;If a Ranger is called contact the Camp Lead on duty immediately
* &#x20;Should someone report and request support for a Consent Violation that did not happen at an event and did not include someone connected to the organization we will do our best to listen and provide resources.
* &#x20;Should someone report a Consent Violation that happened outside of Brulee that involves someone officially connected to either organization (Board Member, Camp Lead, Camp Mate, or Volunteer) we will take that report as outlined in the below procedure and treat it as though it happened at an organization event.
* &#x20;Should the Reporting Party wish to make a formal statement the Interviewer will help them through the process, getting the information they are willing to relate.
* &#x20;Let the Reporting Party know you are following procedure and you will be asking them questions.
  * &#x20;Let the person know they can ask questions in return.
  * &#x20;Be honest with what you know and what you don’t.
* &#x20;Let the Reporting Party know what happens after you are done taking their report.
  * &#x20;A Camp Lead, or Consent Advocate will talk to the Alleged Offender(s).
  * &#x20;A summary of what happened will be generated.
* &#x20;The report and summary will be reviewed by a person designated and trained to deal with consent violations.
* &#x20;Recommendations for any actions to be taken will be made.
* &#x20;Brulee will let them know when recommendations are made.
* &#x20;Let them know that their name will be held in confidence.
* &#x20;Fill out an Incident report form
  * &#x20;Collect names where possible, legal and/or scene names.
  * &#x20;Fill out as much of the form as you can based on what the person tells you.
  * &#x20;Fill out the time & date the report was taken, the time & date the violation/injury happened, and the location where it happened.
* &#x20;Ask if it is okay to contact them for additional information at a later date.
* &#x20;If there are witnesses that voluntarily come forth and want to give a statement, take those statements.

\- There is a place on the Incident Report for them. Where necessary write on a separate sheet and staple to the report.

* If there were camp mates or volunteers present take their names and ask them if they have anything to add.
*   &#x20;After you finish talking to the Reporting Party, and where you feel safe to do so, approach the Alleged Offender and ask them if they would like to make a statement.

    * There is a place on the Incident Report form for taking that statement.
    * &#x20;Understand this person is likely to be upset, confused, scared, angry, or all of the above. Avoid accusatory and judgmental language.

    \- Reassure the Alleged Offender:

    * You are not taking sides. It is your job to collect information and you want to get their side of what happened.
    * Information will be held in confidence by the organization through the review process.
* Answer any questions they have to the best of your ability.

\- Let them know what happens next, same as above

* &#x20; On your discretion you may ask the Alleged Offender to leave the event if:
  * The Consent Violation warrants such in your opinion.
  * If the person’s behavior was, is, or becomes threatening.
* If your safety, the safety of the Reporting Party, or anyone else’s safety is in question.
* &#x20;If either the Reporting Party or the Alleged Offender is not available, unapproachable, or would prefer, statements can be taken at a later time.
* &#x20;Resources
  * Resources and support can be facilitated by contacting the Rangers
* Once the Interviewer fills out an Incident Report, it will be put it in a sealed envelope and either given to the Consent Lead in person or the camp lead on duty.
  * Inform the Consent Lead or Camp Lead on duty immediately that a report has happened.
* Once a CVR is received, the Consent Lead will perform a review or designate someone to perform a review.
  * This review will include:
    * A reading and filing of the given Incident Report and any supporting documentation.
    * Debriefing the person who took the report for any additional information or impressions.
    * A review of the current Master CRV List to see if there are any previous reports regarding the same persons.
    * Contacting the Reporting Party, if they’ve given permission, to get any additional information.
    * Contacting the Alleged Offender, should it seem necessary, to get any additional information.
    * Creation of a brief summary of what happened and the information collected.
      * This Summary should be generated without names. Instead use “Reporting Party”, “Alleged Offender”, “Interviewer”, “Witness 1”, Etc.
      * The Summary should include:
        * Date, event name, location, and time the incident happened.
        * A basic description of what happened from the point of view of the Reporting Party. -A basic description of what happened from the point of view of the Alleged Offender (if available).
      * Where possible, include a brief summary of any witness statements, staff or volunteer impressions, and any previous reports made against the persons involved.
    * Creation of recommendations for actions to be taken. Recommendations might include (but are not limited to):
      * File the report and take no further action.
      * Contact the Reporting Party and offer additional support and/or resources by contacting the Rangers.
      * Contact the Alleged Offender and offer additional support and/or resources by contacting the Rangers.
      * Offer Mediation to both parties to address confusion, misunderstandings, miscommunications, and/or emotional distress. – Both parties must freely agree to this course of action.
      * Give a verbal or written warning to the Alleged Offender.
      * Impose a temporary ban from Organization Events on the Alleged Offender.
      * Impose a temporary ban with a list of conditions which the Alleged Offender must fulfill before being allowed to return to Brulee Events.
      * Impose a permanent ban from all Brulee Events on the Alleged Offender.
      * Make other suggestions as warranted by the Consent Violation.
  * If the report involves anyone directly connected with Brulee or (Camp Mate, Camp Lead, Volunteer, Staff, Board Member, or anyone who the public will see as connected to Brulee) the Consent Lead should:
    * Where there is any perceived conflict of interest, immediately contact the CVR Review Committee to oversee the review process and handle making recommendations with oversight from the Consent Lead.
    * Where there is any perceived conflict of interest, the CVR Review Committee must be made up of people who are not personally or professionally connected to either the Reporting Party or the Alleged Offender.
  * The Consent Lead or designated reviewer will disclose the summary and recommendations to a designated CVR Review Committee
  * The CVR Review Committee will be made up of a minimum of three people drawn from members from the Board of Directors, Camp Leads, and/or trained staff or volunteers.
    * If there is no current CVR Review Committee the Consent Lead can inform the board that one needs to be created immediately.
    * The CVR Review Committee will take in the summary & recommendations, discuss these, ask any additional questions, and create the following within three weeks of the original CVR: - A list of actions to be taken.
      * A statement of how what happened was against the Consent Policy (if it was).
      * A statement of what could have been done differently to comply with the Consent Policy. - A statement of how to request a review or reassessment H5 - Results of the CVR Review Committee’s review created in H2 will be offered to all necessary parties including:
      * The Interviewer and Consent Lead or designated Reviewer. - The Reporting Party and the Alleged Offender.
      * Any Advocate involved in a support role. - The Board, coordinators, and/or other staff only where necessary to understand and/or enforce the actions taken.
    * The CVR Review Committee will close the review process once all parties have been notified.
  * A single copy of the Incident Report, Summary, Statements, and Actions taken will be given to the Consent Lead for storage. - The Consent Lead will store the information in the appropriate locked location, filed by the Alleged Offender’s name, and update the Master CVR List.
    * All other copies of the information should be destroyed
    * A report of the number of Incidents reviewed, actions taken, and potential public relations issues will be given to the Board at the Board Meeting by the chair of the CVR Review Committee.
    * Where names or specific incidents need to be discussed the board may choose to do that in executive session.
    * Anyone involved can request a formal review or reassessment of the recommendations, summary, or decisions made.
  * The Director will forward the request to the standing CVR Review Committee.
    * When forwarded, the Committee will review the request and/or initial documents. After that review they will either confirm the original statement and decisions or issue a new statement and decisions per the above procedure.
  * The request, any statements, changes made, and/or any other documentation will be given to the Consent Lead to be appended to the original file.

### Other Considerations:

#### Concerning Restraining Orders:

\- Legal restraining orders are issued after careful consideration by a judge for a variety of reasons and prevent one party from being within a certain radius of and/or interacting with another party.

\- If a person has a restraining order issued against them, that person should avoid attending any event attended by the person for whom the restraining order is issued and is held responsible for following the order issued against them.

\- If both people have restraining orders, neither should attend any event until the orders are lifted.

\- Brulee will not get involved in any dispute over a restraining order. Should such a dispute happen both parties will be asked to leave the event and associated premises.

\- If, when asked to leave due to a restraining order issue, the person refuses to leave or if it seems clear they were there intentionally, this will be considered a consent violation and the above procedures will be followed.

#### &#x20;If the Consent Violation is a Self-Report:

* &#x20;We recognize not all Consent Violations are intentional. They can happen due to miscommunication, misunderstanding, or simple accident. Some non-consensual behaviors occur due to ignorance, cultural differences, and/or misinformation.
* &#x20;We want to encourage people in acknowledging their own mistakes and seek help in changing difficult, non-consensual, or dangerous behavior.
* &#x20;When the Consent Violation is reported by the person who committed that violation, the reporting procedure outlined above will be followed with the following differences:
  * &#x20;Extra attention should be paid to reassure the person reporting that we appreciate their coming forward themselves.
  * &#x20;The Interviewer and Consent Lead should continue to maintain a nonjudgmental stance and language.
  * &#x20;The person who experienced the violation will be approached carefully and asked if they would like to participate in the report.
  * &#x20;Both parties will be given resources per the basic procedure. Educational resources should be highlighted for the person who self-reported.
  * &#x20;Any review and recommendations should take into account the self-report and lean towards ways to educate and correct.
* &#x20;Even when there is a self-report, it doesn’t excuse the non-consensual behavior. It is still important to focus on and protect the safety of the person who experienced the violation.

### &#x20;Living Document

This Procedure is a living document and will be reviewed and updated as appropriate.
