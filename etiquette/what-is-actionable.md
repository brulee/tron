# 📜 What is Actionable?

### Objective

This Etiquette concerns what is Actionable by our community.  It is not a simple matter of what we care about.  It is a statement of what we can and can not concern ourselves with.  In general, these are objective criteria.

### Etiquette

#### First Party

Out of respect for the Impacted Party's privacy, only incidents where the Impacted Party is directly reporting will be considered actionable.  We will not, for instance, even talk to an Implicated Party based only on a second hand report.

#### Statute of Limitations &#x20;

Incidents that happen more than a year ago are generally NOT actionable.

#### Other Spaces

Other Spaces have different Etiquette.  We cannot be responsible for how anyone behaves outside of Brulee.

#### Relationships

We are **NOT** responsible for member relationships.  This gets a little tricky, and might be the sort of thing requiring Council to think about.

#### Adjudicating Truth

We cannot determine the Truth about what happened.  We acknowledge that many Consent Incidents are very much a they said / they said situation.  Brulee is not equipped to handle evidence and weigh facts.  Our protocols should not ever rely on determining facts.

#### Believe the Impacted Party &#x20;

At the end of the day, the Impacted Party's statement is their view of the facts and it is what we will believe.&#x20;

#### Will of the Impacted Party

If the Impacted Party does not wish for Action for whatever reason, then no Action should be taken.&#x20;

#### Confidentiality of the Impacted Party

If the Impacted Party does not want to be identified or does not want the Implicated Party to be notified, the issue is not actionable.  The first action will almost always be to contact the Implicated Party.

