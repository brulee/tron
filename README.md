# Welcome

Welcome to our community's Etiquette & Protocol server.

📜 "Etiquette" is how to behave.  It is a set of principles and policies that define our community and how we expect you to act.

⚖️ "Protocol" is a set of instructions for how to do something or how to handle something.

🎓 Trainings are the things that must be taught

💔 Impacted Protocols

💞 Empathy Protocols

⚕️ Triage Protocols for Responders
